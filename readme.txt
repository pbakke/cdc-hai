Summary:

This repository contains D3.js code (see index9.html) that creates a U.S. choropleth (map) for CDC-tracked  HAIs. 

See the live version here : http://www.peterbakke.com/bits/cdc-hai/ 

Rational: 

I decided to investigate a Centers for Disease Control and Prevention (CDC) dataset that tracked antibiotic-resistant Healthcare Acquired Infections (HAIs) for 2014 for each state. It took me 2 weeks to wrangle the 161,000-row data file, resulting in a 50-row dataset for each state's number of of reported HAIs. I decided a map of the U.S. was the best way to convey this data and built a story around that map. The map is shaded by rate of HAI per 1000 people. Color-coded maps in this format are called choropleths.

Design:

The start of my data visualization story "slideshow" is located in index.html

The live version of my data story is located on my personal web site here: http://www.peterbakke.com/bits/cdc-hai/index.html

The D3.js code to create my data visualization is located in index9.html.

The files P2.html, P3.html, etc. ('P' stands for 'page') are the "slide" portions of my data story. They contain "previous" and "next" buttons to navigate between story slides. 

CDC = Centers for Disease Control and Prevention
HAI = Hospital Associated (Acquired) Infection 

--

Resources:

Choropleth (Map) example: href="http://bl.ocks.org/NPashaP/a74faf20b492ad377312

Dataset source: https://www.cdc.gov/hai/surveillance/ar-patient-safety-atlas.html 

D3 reference: github.com/d3/d3/wiki

HTML reference: https://www.w3schools.com/tags/default.asp

Programming questions/help: https://stackoverflow.com

Research on HAIs: https://www.cdc.gov/hai/index.html 

Udacity Data Visualization Course: https://www.udacity.com/course/data-visualization-and-d3js--ud507

Peter Bakke peter@bakke.com