Data Dictionary for Data Visualization Project - Peter Bakke

File: cdc_summary11.csv

Fields:

State : string : State abbreviation : used to index 
less1 : numeric : Number of HAIs for age group <1
one_18 : numeric : Number of HAIs for age group 1 to 18
19_64 : numeric : Number of HAIs for age group 19 to 64
65 : numeric : Number of HAIs for age group 65+
SumAges : numeric : Sum of all HAIs 
Population : numeric : Population per state : Used to calculate HAI rate per 1000
less1p1000 : numeric : Rate per 1000 people for age group <1
1_18p1000 : numeric : Rate per 1000 people for age group 1 to 18
19_64p1000 : numeric : Rate per 1000 people for age group 19 to 64
65_p1000 : numeric : Rate per 1000 people for age group 65+
Allagesp1000 : numeric : Rate per 1000 people for all age groups